function compare(a, b) 
{
	if (a.f > b.f) 
		return 1;

	if (a.f < b.f) 
		return -1;
	
	return 0;
}

function PriorityQueue()
{
	this.queue = []
}

PriorityQueue.prototype.push = function(element)
{
	this.queue.push( element )
	this.queue.sort( compare )
}

PriorityQueue.prototype.pop = function()
{
	return this.queue.shift()
}

PriorityQueue.prototype.size = function()
{
	return this.queue.length
}