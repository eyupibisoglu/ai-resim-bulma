module.exports = 
{
	// Fisher Yates Shuffle Algorithm.
	shuffle: function( array )
	{
		var copy = [], n = array.length, i;

		// While there remain elements to shuffle…
		while (n) 
		{

			// Pick a remaining element…
			i = Math.floor(Math.random() * array.length);

			// If not already shuffled, move it to the new array.
			if (i in array) 
			{
				copy.push(array[i]);
				delete array[i];
				n--;
			}
		}

		return copy;
	},

	shuffle2D: function( matrix )
	{
		var array = []
		
		for (var i = 0; i < matrix.length; i++)
			for(var j = 0; j < matrix[i].length; j++)
				array.push( matrix[i][j] )

		var shuffledArray = this.shuffle( array )

		var arrayIterator = 0
		var shuffledMatrix = []

		for (var i = 0; i < matrix.length; i++)
		{
			shuffledMatrix[i] = []

			for (var j = 0; j < matrix[i].length; j++)
			{
				shuffledMatrix[i][j] = shuffledArray[arrayIterator++]
			}
		}

		return shuffledMatrix
	}
};