module.exports = 
{
	generate: function(deep)
	{
		var matrix = []
		var number = 1

		for (var i = 0; i < deep; i++)
		{
			matrix[i] = []

			for (var j = 0; j < deep; j++)
			{
				matrix[i][j] = number++
			}
		}

		return matrix
	}
};