/**
 * HomeController
 *
 * @description :: Server-side logic for managing Homes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = 
{
	index: function(request, response)
	{
		response.view({ title: 'Resim Bulma' })
	},

	matris: function(request, response)
	{

		var matrix = MatrixGeneratorService.generate( 4 )
		matrix[4 - 1][4 - 1] = '*'

		// var shuffledMatrix = ArrayShuffleService.shuffle2D( matrix )
		var shuffledMatrix
		shuffledMatrix = [[7, 2, 4], [5, "*", 6], [8, 3, 1]]
		// matrix = [[1,2,3],[4,5,6],[7,8,"*"]]


		response.view({ title: 'Resim Bulma', image: matrix, deep: request.param('deep') })
	}	
};

